<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


// Get id
$group_id = '';
if(!isset($_GET['group_id']) OR !is_numeric($_GET['group_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
} else {
	$group_id = (int) $_GET['group_id'];
}

$admin = new LEPTON_admin('Pages', 'pages_modify');

//move all fiels in group to no group
$database->simple_query("UPDATE ".TABLE_PREFIX."mod_download_gallery_files SET group_id = '0',  active = '0' WHERE group_id = '".$group_id."' AND section_id = '".$section_id."'");

// Delete row
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_groups WHERE group_id = '".$group_id."' AND section_id = '".$section_id."'");
		
// Initialize order object 
$order = new LEPTON_order(TABLE_PREFIX."mod_download_gallery_groups", 'position', 'group_id', 'section_id');
// reorder all groups in this section_id
$order->clean( $section_id );   

// Say successful
$back_location = ADMIN_URL.'/pages/modify.php?page_id='.$page_id;
$admin->print_success($TEXT['SUCCESS'], $back_location);
// Print admin footer
$admin->print_footer();
