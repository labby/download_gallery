<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


// Get id
$file_id = '';
$fname = '';
if(!isset($_GET['file_id']) OR !is_numeric($_GET['file_id'])) 
{
	header("Location: ".ADMIN_URL."/pages/index.php");
} 
else 
{
	$file_id = (int) $_GET['file_id'];
}

$admin = new LEPTON_admin('Pages', 'pages_modify');

// STEP 1:	Get post details
$query_details = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_download_gallery_files WHERE file_id = ".$file_id." and page_id = ".$page_id,
	true,
	$query_details,
	false
);

if(!empty($query_details)) 
{
	$fname = $query_details['filename'];
	$ext   = $query_details['extension'];
} 
else 
{
	$admin->print_error($TEXT['NOT_FOUND'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// get the file information
$group_id = $query_details['group_id']; // for cleanup sorting ...

//check for multiple entries using the same file name
$query_duplicates = $database->get_one("SELECT count(`file_id`) FROM `".TABLE_PREFIX."mod_download_gallery_files` WHERE `filename` = '".$fname."' and `extension`='".$ext."'");

//only delete the file if there is 1 database entry (not used on multiple sections)
if ($query_duplicates == 1)
{
	// STEP 2:	Delete any files if they exists
	$sTempLink = str_replace(LEPTON_URL, LEPTON_PATH, $query_details['link']);

	if (file_exists($sTempLink)) 
	{
		unlink($sTempLink);
	}
}
// STEP 3:	Delete post
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_files WHERE file_id = ".$file_id);

// STEP 4:	Clean up ordering
if ($group_id == 0)
{
	// apply special id null reorder handling (load extern functions)
	require('functions.php');
	reorder_id_null_group(TABLE_PREFIX."mod_download_gallery_files", $section_id);
	
} 
else 
{			
	// Initialize order object 
	$order = new LEPTON_order(TABLE_PREFIX."mod_download_gallery_files", 'position', 'file_id', 'group_id');
	// reorder all groups in this group_id
	$order->clean( intval($group_id));   
}

// STEP 5:	Say successful
$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
