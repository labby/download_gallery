<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

header('Content-Type: application/javascript');

$aFileOrder = LEPTON_core::getValue("fileOrder", "string", "post", ",", []);

if (empty($aFileOrder))
{
    die("E1: no fileOrder");
}

$sectionID = LEPTON_core::getValue("section_id", "integer", "post");
if (is_null($sectionID))
{
    die("E2: no section");
}

$type = LEPTON_core::getValue("type", "string");

if (is_null($type))
{
    die("E3: no type");
}

$database = LEPTON_database::getInstance();

if ($type == "files")
{
    // "F 0_8,F 1_5,F 1_6,F 2_7,F 2_11,F 3_9"
    $aPositions = [];
    $aTemp = [];
    
    foreach($aFileOrder as $itemRef)
    {
        $aItems = explode("_", substr($itemRef, 2) );
        $group = $aItems[0];
        $id = $aItems[1];
        $aTemp[] = $id;
        if (!isset($aPositions[$group]))
        {
            $aPositions[$group] = 1;
        } else {
            $aPositions[$group]++;
        }
    
        $result = $database->simple_query( "UPDATE `".TABLE_PREFIX."mod_download_gallery_files` set `position` = ".$aPositions[$group]." WHERE `file_id`=".$id );
        if (false === $result)
        {
            die(json_encode($database->get_error()));
        }
    }
    echo json_encode((implode(",",$aTemp)));
}
else {
    echo json_encode("E4: wrong type");
}
