<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php



// Get ID
if (isset($_GET['file_id']) && is_numeric($_GET['file_id']) && isset($_GET['status']) && is_numeric($_GET['status'])) {
	
	$fID 	= intval($_GET['file_id']);
	$status = intval($_GET['status']);
	
}elseif (isset($_GET['group_id']) && is_numeric($_GET['group_id']) && isset($_GET['status']) && is_numeric($_GET['status'])) {
	
	$gID 	= intval($_GET['group_id']);
	$status = intval($_GET['status']);	
	
} else {

	exit(header("Location: ".ADMIN_URL."/pages/index.php"));	
}

// reverse current status to change its value in DB 
$status = ($status == 1) ? 0 : 1;

$admin = new LEPTON_admin('Pages', 'pages_modify');
$update_when_modified = true; // Tells script to update when this page was last updated

// change FILE STATUS?
if (isset($fID) && is_numeric($fID))
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_download_gallery_files` SET `active` = '$status' WHERE `file_id` ='$fID'");
	
// change GROUP STATUS?
if (isset($gID) && is_numeric($gID))
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_download_gallery_groups` SET `active` = '$status' WHERE `group_id` ='$gID'");
	

/**
	return to modify.php
*/
$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id );

// Print admin footer
$admin->print_footer();

