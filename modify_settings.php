<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


//get instance of admin object and own module class
$admin = new LEPTON_admin('Pages', 'pages_modify');
$oDG = download_gallery::getInstance();
$oDG->init_section( $page_id, $section_id );

// include core functions to edit the optional module CSS files (frontend.css, backend.css)
edit_area_settings::getInstance();
LEPTON_handle::register("edit_module_css");

// get edit_area function and get content of template
$edit_area = edit_area::registerEditArea('template_area', 'html');

if (file_exists(LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/download_gallery/view.lte'))
{
	$template_content = file_get_contents(LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/download_gallery/view.lte');
	$template_file = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/download_gallery/view.lte';
} else {
	$template_content = file_get_contents(LEPTON_PATH.'/modules/download_gallery/templates/view.lte');
	$template_file = LEPTON_PATH.'/modules/download_gallery/templates/view.lte';
}

$data = [
    'MOD_DG'            => $oDG->language,
    'action_url'        => LEPTON_URL."/modules/download_gallery/save_settings.php",
    'section_id'        => $section_id,     
    'page_id'           => $page_id, 
    'dg_settings'       => $oDG->dg_settings,
    'dg_file_ext'       => $oDG->dg_file_ext,
    'edit_area'         => $edit_area,		
    'template_content'  => $template_content,
    'template_file'     => $template_file,
    'edit_module_css'   => edit_module_css('download_gallery')
];

/**	
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('download_gallery');
	
echo $oTwig->render( 
	"@download_gallery/modify_settings.lte",	//	template-filename
	$data							//	template-data
);

// Print admin footer
$admin->print_footer();