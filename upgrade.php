<?php
/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


// if module-version < 320
$version = download_gallery::getInstance()->module_version;
if($version < '3.2.0')
{
	// delete obsolete columns 
	$columns = array (
	'use_captcha',	//no userupload
	'userupload',
	'files_loop',	// new output via twig
	'file_header',
	'file_footer',
	'gheader', 
	'gloop', 
	'gfooter', 
	'search_layout' 
	);

	foreach ($columns as $to_delete) {
		$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_download_gallery_settings` DROP COLUMN ".$to_delete." "); 
	}

	// add new column icon in files table
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_download_gallery_files` ADD COLUMN icon VARCHAR(255) AFTER position"); 
	// modify column size in files table
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_download_gallery_files` MODIFY size VARCHAR(32)"); 	
}


// delete obsolete files
$file_names = array(
	'/modules/download_gallery/css/drag_n_drop.css',
	'/modules/download_gallery/dluser_add.php',
	'/modules/download_gallery/dluser_page.php',
	'/modules/download_gallery/dluser_save.php',
	'/modules/download_gallery/footers.inc.php',
	'/modules/download_gallery/backend.css',
	'/modules/download_gallery/backend.js',
	'/modules/download_gallery/backend_body.js',
	'/modules/download_gallery/frontend.css',
);
LEPTON_handle::delete_obsolete_files ($file_names);

LEPTON_handle::delete_obsolete_directories(['/modules/download_gallery/htt']);
