<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


// STEP 0:	initialize some variables
$page_id = (int) $page_id;
$section_id = (int) $section_id;
$fname = '';
$ext = '';

//STEP 1:	Execute query
$query_files = array();
$database->execute_query(
	"SELECT * FROM ". TABLE_PREFIX."mod_download_gallery_files WHERE section_id = ".$section_id." ",
	true,
	$query_files,
	true
	);

//STEP 2:	Check if query has rows
if(!empty($query_files)) 
{
	//STEP 3:	For each file in the page, delete it from the gallery.
	foreach($query_files as $fdetails) {
		$file_id= $fdetails['file_id'];
		$fname = $fdetails['filename'];
		$ext   = $fdetails['extension'];
		//check for multiple evtries using the same file name
		$query_duplicates = $database->get_one("SELECT file_id FROM ".TABLE_PREFIX."mod_download_gallery_files WHERE filename = '".$fname."' and extension='".$ext."'");
		//only delete the file if there is 1 database entry (not used on multiple sections)
		if($query_duplicates != NULL){
			$file = LEPTON_PATH.MEDIA_DIRECTORY.'/download_gallery/' . $fname;
			if(file_exists($file) AND is_writable($file)) { 
				unlink($file);
			}
		}
		//delete file database entry 
		$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_files WHERE file_id = ".$file_id." LIMIT 1");
	}
}

// STEP 4:	Also delete the table entries
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_files WHERE section_id = ".$section_id." ");
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_settings WHERE section_id = ".$section_id." ");
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_file_ext WHERE section_id = ".$section_id." ");
$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_download_gallery_groups WHERE section_id = ".$section_id." ");
