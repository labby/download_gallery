<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


header('Content-Type: application/javascript');

if(!isset($_POST['fileOrder']))
{
    die( "E1: no fileOrder" );
}

if(!isset($_POST['section_id']))
{
    die( "E2: no section" );
}

$database = LEPTON_database::getInstance();

if($_POST['fileOrder']) {
    $aAllGIDS = explode(",", $_POST['fileOrder']);
    $pos = 1;
    $aNewPos = [];
    foreach( $aAllGIDS as $iTempID)
    {
        if($iTempID > 0) {
            $database->simple_query( "UPDATE `".TABLE_PREFIX."mod_download_gallery_groups` set `position` = ".$pos++." WHERE `group_id`=".$iTempID );
        }
    }
   echo json_encode($_POST['fileOrder']);
}