<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


// Get id
$group_id = '';
if(!isset($_GET['group_id']) OR !is_numeric($_GET['group_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
} else {
	$group_id = $_GET['group_id'];
	$page_id = intval($_GET['page_id']);
	$section_id = intval($_GET['section_id']);
}

//get instance of own module class
$oDG = download_gallery::getInstance();
$oDG->init_section( $page_id, $section_id );
$admin = new LEPTON_admin('Pages', 'pages_modify');

// Get header and footer
$query_content = array();
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_download_gallery_groups WHERE group_id = ".$group_id." and page_id = ".$page_id." " ,
	true,
	$query_content,
	false
);	

if (empty($query_content['title']))
	echo '<script type="text/javascript">document.getElementById("title").focus();</script>';

$data = array(
	'MOD_DG' 	=>  $oDG->language,
	'admin_url'	=>	ADMIN_URL,
	'action_url'=>	LEPTON_URL."/modules/download_gallery/save_group.php",
	'section_id'=>	$section_id,     
	'page_id'	=>	$page_id, 
	'group_id'	=> $group_id,
	'active'	=> $query_content['active'],
	'title'		=> stripslashes($query_content['title'])
	);

/**	
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('download_gallery');
	
echo $oTwig->render( 
	"@download_gallery/modify_group.lte",	//	template-filename
	$data							//	template-data
);

// Print admin footer
$admin->print_footer();
