<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */
 
class download_gallery extends LEPTON_abstract
{
    public array $icon_image = [];
    public array $dg_settings = [];
    public array $dg_groups = [];
    public array $dg_files = [];
    public array $dg_file_ext = [];
    
    public LEPTON_database $database;
    static $instance;

    public function initialize() 
    {
        $this->database = LEPTON_database::getInstance();
    }
    
    public function init_section( $iPageID = 0, $iSectionID = 0 )
    {
        $database = LEPTON_database::getInstance();

        // start define image icons
        $database->execute_query(
            "SELECT * FROM ".TABLE_PREFIX."mod_download_gallery_file_ext WHERE section_id = '".$iSectionID."' and page_id = '".$iPageID."' ORDER BY file_image " ,
            true,
            $this->dg_file_ext,
            true
        );

        //get array of icons and filetypes
        foreach ($this->dg_file_ext as &$icon) {
            $this->icon_image[ $icon['file_image'] ] = explode(",", $icon['extensions']);
        }
        
        //get array of settings
        $database->execute_query(
            "SELECT * FROM ".TABLE_PREFIX."mod_download_gallery_settings WHERE section_id = '".$iSectionID."' and page_id = '".$iPageID."' ",
            true,
            $this->dg_settings,
            false
        );    
        
        // get array of groups
        $database->execute_query(
            "SELECT * FROM ".TABLE_PREFIX."mod_download_gallery_groups WHERE section_id = '".$iSectionID."' and page_id = '".$iPageID."' ORDER BY `position`" ,
            true,
            $this->dg_groups,
            true
        );    
        
        // add no group to groups array
        $this->dg_groups = array_merge(
            [
                [
                    'group_id'  => 0,
                    'title'     => $this->language['NOGROUP'],
                    'position'  => 0
                ]
            ],
            $this->dg_groups
        );            

        // get array of files
        $this->dg_files = [];      //reset array
        $database->execute_query(
            "SELECT * FROM ".TABLE_PREFIX."mod_download_gallery_files WHERE section_id = '".$iSectionID."' and page_id = '".$iPageID."' ORDER BY `position`" ,
            true,
            $this->dg_files,
            true
        );
            
    }
    
    public function get_file_extension($file_extension ='') 
    {
        $file_image = ''; // initialize var    
        foreach ($this->icon_image as $file_key =>$file_icon_type_list)
        {  // get the matching file_icon
            if (in_array ($file_extension, $file_icon_type_list))
            {
                $file_image = $file_key;
                break;
            }
        }
        return $file_image;
    }    
    
    /**
     *  Get the file size of the a given local file with the "human" format in b/kb/mb/tb.  
     *
     *  @param string   $file_link  A valid path to the file.
     *  @param integer  $settings_file_size_decimals    Number of digits, default is 0
     *
     *  @return string  The result as string or 0;
     */
    public function get_file_size($file_link='',$settings_file_size_decimals='0') 
    {
        $sLocalPath = str_replace(LEPTON_URL,LEPTON_PATH,$file_link);
        if (!file_exists($sLocalPath))
        {
            return "File not found! [".$sLocalPath."]";
        }
        $result = filesize($sLocalPath); 
        if ($result == false)
        {
            return 0; // M.f.i.
        }
        return $this->human_file_size($result,$settings_file_size_decimals);
    }

    /**
     *  Get the file size of the a given external file with the "human" format in b/kb/mb/tb. 
     *
     *  @param  string  $file_url   A valid url to the file looking for.
     *  @param integer  $settings_file_size_decimals    Number of digits, default is 0
     *
     *  @return string  The result as string or 0;
     */
    public function get_external_file_size($file_url='',$settings_file_size_decimals='0') 
    {
        
        if ($file_url === "")
        {
            return "Error: url to external file is empty!";
        }
        
        if (!$fp = fopen( $file_url , 'r'))
        {
            trigger_error("Unable to open URL (".$file_url.")", E_USER_ERROR);
        }
        $meta = stream_get_meta_data($fp);
        fclose($fp);

        $length = 0;
        foreach($meta['wrapper_data'] as $temp_line)
        {
            if(0 === strpos( $temp_line, "Content-Length: " ))
            {
                $length = intval( str_replace("Content-Length: ", "", $temp_line ) ); // insert value
                
                break;
            }
        }
        
        return $this->human_file_size($length,$settings_file_size_decimals);    
    }    

    // make human readable filesize (bytes)
    public function human_file_size($bytes, $precision = 2)
    {
        $name = array('Bytes','KB','MB','GB','TB');
        
        if (!is_numeric($bytes) || $bytes < 0) 
            return false;
            
        for ($level = 0; $bytes >= 1024; $level++) 
            $bytes /= 1024;
        
        return round($bytes, $precision) . ' ' . $name[$level];
    }    
    
} // end of class
