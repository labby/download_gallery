<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php



$file = ''; 
$dlcount = '';
if(!isset($_GET['file']) || !is_numeric($_GET['file'])) {

	header('Location: ../index.php');
	
}else{

	$file = intval($_GET['file']);
	
}
if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	
	header('Location: ../index.php');
	
}else{

	$prove = intval($_GET['id']);
	
}

/**
	Query File
*/
$fetch_file = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_download_gallery_files` WHERE `file_id` = ".$file." AND `modified_when` = ".$prove." ",
	true,
	$fetch_file,
	false
);
if (empty($fetch_file)) 
{	
	header('Location: ../index.php');	
}

$page_info = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$fetch_file['page_id']." ",
	true,
	$page_info,
	false
);

// check download permissions:
$dl_allowed = false;
if ($page_info['visibility'] == 'public' || $page_info['visibility']=="hidden") 
{	
	$dl_allowed = true;	
}
	
if (!$dl_allowed) 
{
	if ((isset($_SESSION['USER_ID']) && $_SESSION['USER_ID'] != "" && is_numeric($_SESSION['USER_ID']))
	&& ($page_info['visibility']=="registered" ||  $page_info['visibility']=="private")) 
	{
		$groups = explode(",", $page_info['viewing_groups']);
		foreach (explode(",", $_SESSION['GROUPS_ID']) as $cur_group_id) 
		{
			if (in_array($cur_group_id, $groups)) 
			{
				$dl_allowed = true;
			}
		}
	}
}

if ($dl_allowed) 
{	
    // increment download counter:
	$dlcount = $fetch_file['dlcount']+1;
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_download_gallery_files` SET `dlcount` = ".$dlcount." WHERE `file_id` = ".$file." ");

	// deliver the file:
	$orgfile = $fetch_file['link'];
	header('Location: '.$orgfile);
	
} 
else 
{
	echo "No access!";	
}
