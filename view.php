<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php


$oDG = download_gallery::getInstance();
$oDG->init_section( $page_id, $section_id );

//  Aldus: 2024-09-25 - why this?
require_once LEPTON_PATH.'/modules/download_gallery/info.php';

// get file extension for each file
foreach ($oDG->dg_files as &$temp)
{
    /*
     * Loop through all files to get values for .lte
     */
	if ($temp['link'] != $temp['filename'] )
	{   
	    //get filesize and icons of internal files
		$file_image = $oDG->get_file_extension($temp['extension']);
		$temp['file_ext'] = '<img alt="'.LEPTON_URL.'/modules/download_gallery/images/'.$file_image.'" src="'.LEPTON_URL.'/modules/download_gallery/images/'.$file_image.'" />';
		
		$database->simple_query("UPDATE ".TABLE_PREFIX."mod_download_gallery_files SET `icon` = '".$temp['file_ext']."' WHERE `file_id` = '".$temp['file_id']."' ");
		
		$mTempResult = $oDG->get_file_size($temp['link'], $oDG->dg_settings['file_size_decimals']);
		
		$temp['size'] = (strPos( $mTempResult, "File not found" ) !== false) 
		    ? 0
		    : $oDG->get_file_size($temp['link'], $oDG->dg_settings['file_size_decimals'])
		    ;

		$database->simple_query("UPDATE ".TABLE_PREFIX."mod_download_gallery_files SET `size` = '".$temp['size']."' WHERE `file_id` = '".$temp['file_id']."' ");

		$temp['link'] = LEPTON_URL . '/modules/download_gallery/dlc.php?file=' .$temp['file_id'].'&amp;id='.$temp['modified_when'];
	}
	else
	{
	    //get filesize and icons of external files
		$get_extern_icon = strtolower(substr( strrchr($temp['filename'],'.'),1));
		$file_image = $oDG->get_file_extension($get_extern_icon);
		$temp['file_ext'] = '<img src="'.LEPTON_URL.'/modules/download_gallery/images/'.$file_image.'" />';  
		$database->simple_query("UPDATE ".TABLE_PREFIX."mod_download_gallery_files SET `icon` = '".$temp['file_ext']."' WHERE `file_id` = '".$temp['file_id']."' ");
		
		
		$temp['size'] = $oDG->get_external_file_size( $temp['link'], $oDG->dg_settings['file_size_decimals']);
		$database->simple_query("UPDATE ".TABLE_PREFIX."mod_download_gallery_files SET `size` = '".$temp['size']."' WHERE `file_id` = '".$temp['file_id']."' ");
		
		$temp['link'] = LEPTON_URL . '/modules/download_gallery/dlc.php?file=' .$temp['file_id'].'&amp;id='.$temp['modified_when'];
	}
}


$files_in_group = [];
foreach ($oDG->dg_files as &$temp)
{
	$files_in_group[$temp['group_id']] = $files_in_group[$temp['group_id']] ?? 1;
}
//echo(LEPTON_tools::display($oDG->dg_settings['search_filter'],'pre','ui message'));

// data for twig template engine	
$data = [
    'addon'         => $oDG,
    'search_filter' => $oDG->dg_settings['search_filter'],
    'files_in_group'=> $files_in_group,
    'dg_link'       => LEPTON_URL.'/modules/download_gallery/dlc.php',	
    'dateformat'    => str_replace(' ','/', DEFAULT_DATE_FORMAT),
    'addon_name'    => $oDG->module_name
];

/**	
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('download_gallery');
	
echo $oTwig->render( 
	"@download_gallery/view.lte",	//	template-filename
	$data							//	template-data
);	
