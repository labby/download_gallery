<?php

/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$admin = new LEPTON_admin('Pages', 'pages_modify');

// Get group id
$group_id = $admin->getValue('group_id', "integer");
if (is_null($group_id))
{
    header("Location: ".ADMIN_URL."/pages/index.php");
}

// Get active
$active = $admin->getValue("active", "integer");
if (is_null($active))
{
	header("Location: ".ADMIN_URL."/pages/index.php");
}

// get title
if ($admin->getValue('title') == '')
{
	$admin->print_error($MESSAGE['GENERIC']['FILL_IN_ALL'], LEPTON_URL.'/modules/download_gallery/modify_group.php?page_id='.$page_id.'&section_id='.$section_id.'&group_id='.$group_id);
} else {
	$title = addslashes(strip_tags($admin->getValue('title')));
}

// Update db entry
$fields = [
    'title' => $title,
    'active' => $active
];

$database->build_and_execute(
    'update',
    TABLE_PREFIX."mod_download_gallery_groups",
    $fields,
    "`group_id`=".$group_id
);
// $database->simple_query("UPDATE ".TABLE_PREFIX."mod_download_gallery_groups SET title = '$title', active = '$active' WHERE group_id = '$group_id' and page_id = '$page_id'");


// Check if there is a db error, otherwise say successful
if ($database->is_error())
{
	$admin->print_error($database->get_error(), LEPTON_URL.'/modules/download_gallery/modify_group.php?page_id='.$page_id.'&section_id='.$section_id.'&group_id='.$group_id);
} else {
	$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// Print admin footer
$admin->print_footer();
