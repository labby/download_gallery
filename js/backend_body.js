/**
 *  @module			Download Gallery
 *  @version		see info.php of this module
 *  @authors		Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @copyright		2010-2024 Hudge, Woudloper, M. Gallas, R. Smith, C. Sommer, F. Heyne, Aldus, erpe
 *  @license		GNU General Public License
 *  @license terms	see info.php of this module
 *  @platform		see info.php of this module
 *
 */
 
// see: https://api.jqueryui.com/sortable/#event-update
$("tbody").sortable({
    items:      "> tr:not(:first)[id^='F ']",
    appendTo:   "parent",
    helper:     "clone",
    update: function(event, ui) {
            var fileOrder = $(this).sortable('toArray').toString();
            var result = $.post( LEPTON_URL+'/modules/download_gallery/update_sort.php',
                {
                    leptoken: LEPTOKEN,
                    section_id: SECTION_ID,
                    fileOrder:fileOrder,
                    type: 'files',
                    success: function(a,b) {
                            // console.log("send "+a )
                        },
                    done: function(a,b) {
                        // console.log("res ");
                    }
                },
                function(response)
                {
                    // alert(response);
                    // console.log("response: "+response);
                    
                    $("#dg_response_message").show();
                    $("#dg_response_message").text("New sort: "+ response.replace('\\"', ""));
                }
            )
        } 
    }
).disableSelection();

